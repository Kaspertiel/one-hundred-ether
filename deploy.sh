#!/usr/bin/env sh

# Set environment variables
STACK_NAME=${STACK_NAME:-one-hundred-ether-wall}
STACK_ENV=${STACK_ENV:-dev}
STACK=${STACK_NAME}-${STACK_ENV}
AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:-eu-west-1}

aws cloudformation deploy \
    --template-file build_webpack/deployment.yaml \
    --stack-name ${STACK} \
    --capabilities CAPABILITY_IAM \
    --parameter-overrides StackEnv=${STACK_ENV} \
    > /dev/null \
    && echo "Package was successfully deployed."

# Sync client files with S3 Bucket
echo "Syncing client with S3 Bucket..."
echo ${STACK}
aws s3 sync build_webpack s3://${STACK}-static/ --acl public-read \
  && echo "Successfully synced client with S3 Bucket."

 
