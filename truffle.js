var HDWalletProvider = require("truffle-hdwallet-provider");
var mnemonic = process.env.MNEMONIC;
var token = process.env.TOKEN;

//deploying the contract makes use of the HDWalletProvider.
//in order to deploy a contract, use the truffle command:
//truffle migrate --network {networkname}
//the migration requires a payment so you must give a mnemonic
//in the cmd to the enviroment in order to unlock you wallet

//To connect with the infura nodes, a token is required.
//please make sure to pass the token to the env variables aswell.
module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*", // Match any network id
    },
    ropsten: {
      provider: new HDWalletProvider(mnemonic, "https://ropsten.infura.io/" + token),
      network_id: 3,
      gas: 4500000,
      gasPrice: 25000000000,
    },
    rinkeby: {
      provider: new HDWalletProvider(mnemonic, "https://rinkeby.infura.io/" + token),
      network_id: 4,
      gas: 4500000,
      gasPrice: 25000000000,
    },
    // rinkeby: {
    //   provider: new HDWalletProvider(mnemonic, "http://localhost:7545"),
    //   network_id: 4,
    //   gas: 4500000,
    //   gasPrice: 25000000000,
    // },
    live: {
      provider: new HDWalletProvider(mnemonic, "https://mainnet.infura.io/" + token),
      network_id: 1,
      gasPrice: 15000000000,
      gas: 4500000,
    },
    // live: {
    //   provider: new HDWalletProvider(mnemonic, "http://localhost:7545"),
    //   network_id: 1,
    //   gasPrice: 12000000000,
    //   gas: 4500000,
    // },
  }
};
