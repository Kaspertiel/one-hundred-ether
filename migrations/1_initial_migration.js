var Migrations = artifacts.require("./Migrations.sol");

// module.exports = function(deployer) {
//   deployer.deploy(Migrations);
// };

module.exports = function(deployer) {
  deployer.deploy(Migrations)
  .then(() => Migrations.deployed())
  .then(migrations => new Promise(resolve => setTimeout(() => resolve(migrations), 6000)));
};
