//this pixelprice means 0.00008 ETH per pixel. 25 x 25 x 0.00008 ETH = 0.05 ETH per block.
//if you want to change the pixelprice please make sure to ajust the one ine the smart contract aswell.
export const weiPixelPrice = 80000000000000;

//this is the highest gasprice used for purchasing an ad
export const highGasPrice = 4000000000;

//this is the lower gasprice used for updating an ad and buying an ad from the marketplace
export const midGasPrice = 2000000000;

//this is the lowest gasprice used for withdrawing funds, selling an ad
//in the Marketplace and setting an ad inactive
export const lowGasPrice = 1000000000;

//besides the gasprice theres also a gas limit. this is the high one
export const highGasLimit = 1000000;

//this is the low gas limit
export const lowGasLimit = 84968;

//size of an block in pixels
//not if you want to change this you should change the contract aswell
export const blockSize = 25 * 25;

//price of one block in ether
export const blockPrize = 0.05;
