//HomePage (container)
//-----------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------
export const startSelectButtonText = "Start selecting!";

export const clearSelectButtonText = "Clear selection";

export const manageAdsButtonText = "Manage Ads";

export const withdrawButtonText = "Withdraw";

//Header (component)
export const headerTitleLeft = "How does it work?";

export const headerTextLeft = "Just drag-select empty space in the grid and you're on your way!";

export const headerTitleMid = "The One Hundred Ethereum Wall";

export const headerNetwork = "Network: ";

export const headerTitleRight = "Mining";

export const headerTextRight = "It might take a while before your transaction is mined, be patient!";

//InfoModal (component)
export const infoModalTitle = "Pricing and scaling";

export const infoModalExp1 = "Size of one block: 25 x 25 pixels";

export const infoModalExp2 = "Price of one block: 0.05 ETH";

export const infoModalExp3 = "Minimum purchase: 1 block";

export const infoModalExp4 = "Total blocks in grid: 2000";

export const infoModalExp5 = "Total sold blocks: ";


//SelectableGrid(component)
export const imageInactive = "This image has been flagged inactive";

export const unsupportedNetworkTitle = "You must either be on the Ethereum Main network or the Rinkeby test network to view and use this application.";

export const unsupportedNetworkExp = "You can switch networks by opening the MetaMask extension and selecting the correct network in the upper left corner";

export const unsupportedWeb3Exp = "Your browser does not support web3 yet. In order to view and use this application you should install a Web3 provider like MetaMask.";

//Form(component)
export const formTitle = "Enter information";

export const formExp1 = "In order to have your image displayed, enter the title and link of your website and choose a valid image: ";

export const formExp2 = "Please make sure to include either http:// or https:// ";

export const formExp3 = "please make sure to include a valid extension: jpeg, jpg, png, gif, ico ";

export const formExp4 = "Please make sure to keep your imagesize under 300 KB";

export const formTitleLabel = "Title";

export const formLinkLabel = "Website Link";

export const formImageLabel = "Choose image";

export const formTitlePlaceholder = "Text you want to see the visitors when hovering over your image";

export const formLinkPlaceholder = "e.g. https://www.myawesomecompany.com";

export const fileSizeTooLarge = "(file is too large, keep below 300KB)";

export const fileSizeWithinLimit = "(file is within the limits)";

export const formPrevButton = "Previous";

export const formBuyButton = "Buy";

//priceOverview (component)

export const priceOverViewTitle1 = "You are about to buy ";

export const priceOverViewTitle2 = " block";

export const priceOverViewTitle3 = " blocks";

export const priceOverviewTableTitle = "Overview";

export const overviewUnit = "Per unit";

export const overviewSelection = "Selection";

export const overviewBlocks = "Blocks";

export const overviewPixelsCaps = "Pixels";

export const overviewPixels = " pixels";

export const overviewPosition = "Position";

export const overviewColumn = "Column: ";

export const overviewRow = " Row: ";

export const overviewGasfee = "Gas fee";

export const overviewEst = "est 0.001650 ETH";

export const overviewSuggest = "Taking too long? try raising the gas price";

//manageAdsForm (component)
export const manageAdsTitle = "Manage ads";

export const manageAdsExp = "Your purchased ads can be managed down here. You can select the ad you would would like to change and put in new data in the inputfields. " +
                            "Whenever you save the changes, your payment provider will pop up again asking your confirmation. Be aware that changing your data " +
                            "adds a new piece of block on the blockchain and therefore requires a small gas fee to be mined.";

export const manageAdsNote = "Note:";

export const manageAdsAdLabel = "Ad";

export const manageAdsTitleLabel = "Title";

export const manageAdsLinkLabel = "Website Link";

export const manageAdsCurrentImgLabel = "Current image";

export const manageAdsNewImgLabel = "Choose New image";

export const manageAdsSave = "Save changes";

export const manageAdsActive = "Set active";

export const manageAdsInactive = "Set inactive";

export const manageAdsInactiveDrop = " (inactive)";

//MarketPage (container)
//-----------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------
export const marketTitle = "Marketplace";

export const marketUnderTitle = "Buy and sell ads in the market!";

//ManageAdsSale (component)
export const manageSaleAdLabel = "Select your ad";

export const manageSalePriceLabel = "Set your price";

export const ethLabel = "ETH";

export const marketSaleButton = "Sell selected";

export const marketCancelButton = "Cancel sale selected";

export const marketTablePosition = "X / Y Position";

export const marketTableSize = "Width / Height";

export const marketTableSale = "For sale?";

export const marketTablePreview = "Preview";

export const dropdownForSale = " (Ad is for sale)";

//marketTable (component)
export const marketTableMainTitle = "Ads for sale";

export const marketTableTitleField = "Title";

export const marketTablePosField = "X / Y position";

export const marketTableSizeField = "Width / Height";

export const marketTableOwnerField = "Owner";

export const marketTablePriceField = "Price";

export const marketTablePreviewField = "Preview";

export const marketTableActionsField = "Actions";

export const marketTableXContainer = "X: ";

export const marketTableYContainer = " Y: ";

export const marketTableWidthContainer = "Width: ";

export const marketTableHeightContainer = " Height: ";

export const marketTablePriceContainer = " ETH";

export const marketTableNoAds = "No ads for sale";


//AboutPage (container)
//-----------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------
export const aboutTitle = "Welcome";

export const underTitle = "To the One hundred Ethereum wall!";

export const exp1 = "This project was created in order to exploit some new techniques concerning blockchain. "  +
                    "In this decentralized app you can purchase a piece of the Hundred Ethereum Wall. "  +
                    "The wall is devided by 2000 blocks each containing 25 x 25 pixels (625) at the price "  +
                    "of 0.05 Ethereum each. If all blocks are sold out the wall is full, no new "  +
                    "blocks can be bought anymore. However, you can still trade them at the marketplace. " +
                    "You can also ajust your purchased spot at any time, changing the title, link and image of the ad. " +
                    "As said this project was created to develop experience in new techniques. If you want to make your " +
                    "own dApp with blockchain and Ethereum transactions, feel free to use the code provided in git as " +
                    "you please. In addition, you could choose to follow our guidance using the following tools for your app:";

export const truffleTitle = "Truffle framework + Ganache";

export const exp2 = "For starters, you should realy look at the Truffle framework. As you are about the create a smart contract for " +
                    "whatever it is you are selling, you might aswell make your life easy and use Truffle. Truffle comes with build in " +
                    "smart contract compilation, linking, deployment and binary management. If you have worked with smart contract before, " +
                    "you might have experienced that can be a pain in the ass. Furthermore it comes with tutorials and examples on how to write " +
                    "and talk to smart contracts. It has a good Truffle react box, in which you can start your own project. Truffle also provides an " +
                    "easy way to test your dApp on Ganache. Ganache hosts a local blockchain on your machine with a lot of perks like; automining, " +
                    "testaccounts with Ether and an easy to use UI.";

export const truffleLink = "http://truffleframework.com/";

export const truffleLinkName = "truffleframework.com";

export const metamaskTitle = "MetaMask";

export const exp3 = "In order to purchase anything with Ethereum and sign your blockchain transaction, a payment provider is needed. MetaMask is one of " +
                    "those providers you can choose from. Its an easy to use browser extension in which you can import (or create) your Ethereum wallet. " +
                    "Whenever you initialize a puchase in your dApp, a MetaMask window pops up to show you some metadata of the transaction. From there you " +
                    "can complete the transaction. They also allow you to open up your localhost network and adapt all the accounts that Ganache generated for you. ";

export const metaLink = "https://metamask.io/";

export const metaLinkName = "metamask.io";

export const web3Title = "Web3.js";

export const exp4 = "Web3 is an Ethereum JavaScript API. It contains a collection of libararies which allow you to interact with the Ethereum blockchain. Web3.js is a must " +
                    "have if you want to write to the blockchain. Some examples on how to use the library can be found in the Truffle example boxes and ofcourse their documentation. " +
                    "Note that Web3 is implementable in more then just JavaScript. For example; Web3.py.";

export const web3Link = "https://web3js.readthedocs.io/en/1.0/";

export const web3LinkName = "web3js.readthedocs.io";

export const ipfsTitle = "IPFS";

export const exp5 = "To make the app even more decentralized, the Inter Planetary File System (IPFS) is used for storage of the images. These images are deplyed by the app on the peer-to-peer " +
                    "network IPFS has to offer. Deploying files to IPFS returns a hash unique to that datafile. This hash is then captured in the blockchain along with the rest of the ad data.";

export const ipfsLink = "https://ipfs.io/";

export const ipfsLinkName = "ipfs.io";

//HelpPage (container)
//-----------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------
export const helpTitle = "How to?";

export const purchaseTitle = "Purchase ad";

export const changeTitle = "Change ad";

//Notifications (component)
//-----------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------
export const messageNoAds = "You dont have any ads to manage";

export const messageSpotTaken = "Oops! That spot is taken..";

export const messageUnlockWallet = "You must unlock your wallet to take action!";

export const messageNotDeployed = "The contract was not deployed to this network";

export const messagePurchaseReq = "Purchase requested";

export const messagePurchaseDec = "Purchase declined";

export const messageUpdateReq = "Update requested";

export const messageUpdateDec = "Update declined";

export const messageActionReq = "Action requested";

export const messageActionDec = "Action declined";

export const messageWithdrawReq = "Funds withdrawal requested";

export const messageWithdrawDec = "Funds withdrawal declined";

export const messageTxComplete = "Transaction complete";

export const messageSaleReq = "Sale requested";

export const messageSaleDec = "Sale declined";

export const messageCancelSaleReq = "Cancel sale requested";

export const messageCancelSaleDec = "Cancellation declined";

//Footer (component)
//-----------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------
export const footerBlock1Title = "Who?";

export const footerBlock1Item1 = "Moddix";

export const footerBlock1Item2 = "Tiel";

export const footerBlock2Title = "What?";

export const footerBlock2Item1 = "Home";

export const footerBlock2Item2 = "Marketplace";

export const footerBlock2Item3 = "Project";

export const footerBlock2Item4 = "GIT";

export const footerBlock2Item5 = "Smart contract";

export const footerBlock3Title = "How?";

export const footerBlock3Item1 = "Purchase ad (video)";

export const footerBlock3Item2 = "Change ad (video)";

export const footerBlock4Title = "Partners";

export const footerBlock4Item1 = "State of the ÐApps";

//app (main)
//-----------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------
export const navLink1 = "Home";

export const navLink2 = "Marketplace";

export const navLink3 = "About";

export const navLink4 = "Help";
