import React from 'react';
import Modal from 'react-responsive-modal';
import '../styles/modal.css';
import {Button} from 'react-bootstrap';
import Form from './Form'
import PriceOverview from './PriceOverview'

class BuyModal extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      open: false,
      formOpen: false
    };
    this.onOpenModal = this.onOpenModal.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
    this.onOpenForm = this.onOpenForm.bind(this);
    this.onCloseForm = this.onCloseForm.bind(this);
  }

  render() {
    const open = this.state.open;

    let buttonText = this.props.buttonText;
    let modalOverview = <PriceOverview
                          selectedItems={this.props.selectedItems}
                          onOpenForm={this.onOpenForm}
                        />;
    let modalForm = <Form
                      selectedItems={this.props.selectedItems}
                      onCloseForm={this.onCloseForm}
                      onCloseModal={this.onCloseModal}
                      buy={this.props.buy}
                    />;

    let loadContent = this.state.formOpen ? modalForm : modalOverview;

    return (
      <div className="inner">
        <Button bsStyle="success"
          style={this.props.style}
          className={this.props.className}
          onClick={this.onOpenModal}
          disabled={this.props.disabled}
        >
          {buttonText}
        </Button>

        <Modal open={open} onClose={this.onCloseModal} little>
          {loadContent}
        </Modal>
      </div>
    );
  }

  onOpenModal() {
    this.setState({ open: true, formOpen: false });
  }

  onCloseModal() {
    this.setState({ open: false });
  }

  onOpenForm() {
    this.setState({ formOpen: true });
  }

  onCloseForm() {
    this.setState({ formOpen: false });
  }
}

export default BuyModal;
