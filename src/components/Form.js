import React from 'react';
import '../styles/modal.css';
import {Button} from 'react-bootstrap';
import ipfs from '../utils/ipfs';
import { RingLoader } from 'react-spinners';
import * as strings from '../config/strings';

class Form extends React.Component {

	constructor (props) {
    super(props);
    this.initiateBuy = this.initiateBuy.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleLinkChange = this.handleLinkChange.bind(this);
		this.validateUrl = this.validateUrl.bind(this);
		this.captureFile = this.captureFile.bind(this);
		this.convertToBuffer = this.convertToBuffer.bind(this);

    this.state = {
			title: '',
			link: '',
			linkCorrect: false,
			imageCorrect: false,
			message: null,
			toggle: false,
			ipfsHash: null,
			buffer: '',
			loading: false,
			fileSize: null,
    };
  }

	render() {
		return (
			<div>
				<div className='sweet-loading'>
					<RingLoader
						color={'#65cdf1'}
						loading={this.state.loading}
					/>
				</div>
				<div className='h2Wrapper'>
					<h2 className="modalH2">{strings.formTitle}</h2>
				</div>
				<h4>{strings.formExp1}<br /> <br />
				{strings.formExp2}<br />
				{strings.formExp3}<br />
				{strings.formExp4}</h4>

				<form className="form-horizontal">
					<div className="form-group">
						<label className="col-sm-2 control-label" htmlFor="title">{strings.formTitleLabel}</label>
						<div className="col-sm-10">
							<input
								type="text"
								className="form-control"
								id="title"
								placeholder={strings.formTitlePlaceholder}
								onChange={this.handleTitleChange}
								maxLength="70"
							/>
						</div>

						<label className="col-sm-2 control-label" htmlFor="link">{strings.formLinkLabel}</label>
						<div className="col-sm-10">
							<input
								type="text"
								className="form-control"
								id="link"
								placeholder={strings.formLinkPlaceholder}
								onChange={this.handleLinkChange}
							/>
						</div>

						<label className="col-sm-2 control-label" htmlFor="image">{strings.formImageLabel}</label>
						<div className="col-sm-10">
							<input type="file" className="custom-file-input" accept="image/*" onChange={this.captureFile}/>
						</div>

						<label className="col-sm-2 control-label" htmlFor="size"></label>
						<div className="col-sm-10">
							<p>
								File size: {Math.round(this.state.fileSize)} KB {this.state.fileSize > 300 ?
								strings.fileSizeTooLarge : strings.fileSizeWithinLimit}
							</p>
						</div>

					</div>
				</form>

				<div className='buttonGroup'>
					<div className="pull-left">
						<Button bsStyle="default" className="prevButton" onClick={this.props.onCloseForm}>{strings.formPrevButton}</Button>
					</div>
					<div className="pull-right">
						<Button
							bsStyle="success"
							className='buyButton submit'
							onClick={this.initiateBuy}
							disabled={!this.state.imageCorrect || !this.state.linkCorrect || this.state.fileSize > 300} >{strings.formBuyButton}
						</Button>
					</div>
				</div>
			</div>
		);
	}

	initiateBuy() {
		this.setState({ loading: true });

		ipfs.add(this.state.buffer, (err, ipfsHash) => {
			//setState by setting ipfsHash to ipfsHash[0].hash
			// this.setState({ ipfsHash: ipfsHash[0].hash });
			if (ipfsHash[0].hash) {
				this.props.onCloseModal();
				this.props.buy(this.state.title, this.state.link, ipfsHash[0].hash);
				this.setState({ loading: false });
			}
		})
	}

	handleTitleChange(e) {
		this.setState({title: e.target.value});
	}

	handleLinkChange(e) {
		let link = e.target.value;
		this.setState({link: link});
		let validLink = this.validateUrl(link);
		if (validLink) {
			this.setState({linkCorrect: true})
		} else {
				this.setState({linkCorrect: false})
		}
	}

	validateUrl(link) {
		let substring = "https://";
		let substring2 = "http://";

		if (link.includes(substring) || link.includes(substring2)) {
			return true;
		} else {
				return false;
		}
	}

	validateImageExtension(url){
		return(url.match(/\.(jpeg|jpg|gif|png|ico)$/) != null);
	}

	captureFile(e) {
		try {
			e.stopPropagation();
			e.preventDefault();
			const file = e.target.files[0];
			this.setState({fileSize: file.size / 1024 })
			let validExtension = this.validateImageExtension(file.name);

			if (validExtension) {
				this.setState({imageCorrect: true})
			} else {
				this.setState({imageCorrect: false})
			}

			let reader = new window.FileReader();
			reader.readAsArrayBuffer(file);
			reader.onloadend = () => this.convertToBuffer(reader);
		} catch (error) {
			//
		}
  }

	async convertToBuffer(reader) {
		 //file is converted to a buffer for upload to IPFS
			 const buffer = await Buffer.from(reader.result);
		 //set this buffer -using es6 syntax
			 this.setState({buffer});
	}
}

export default Form;
