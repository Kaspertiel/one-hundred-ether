import React from 'react';
import '../styles/footer.css';
import {NavLink} from 'react-router-dom';
import * as strings from '../config/strings';

const Footer = () => {
  return (
    <footer className="parent">
      <div className="child">
        <h4 className="title">{strings.footerBlock1Title}</h4>
        <a href="https://www.moddix.com/" target="blank"><h5 className="items">{strings.footerBlock1Item1} </h5></a>
        <a href="https://www.linkedin.com/in/kasper-tiel-455106159/" target="blank"><h5 className="items">{strings.footerBlock1Item2}</h5></a>
      </div>
      <div className="child">
        <h4 className="title">{strings.footerBlock2Title}</h4>
        <NavLink className="nav_link" to="/" ><h5 className="items">{strings.footerBlock2Item1}</h5></NavLink>
        <NavLink className="nav_link" to="/market" ><h5 className="items">{strings.footerBlock2Item2}</h5></NavLink>
        <NavLink className="nav_link" to="/about" ><h5 className="items">{strings.footerBlock2Item3}</h5></NavLink>
        <a href="https://gitlab.com/Kaspertiel/one-hundred-ether" target="blank"><h5 className="items">{strings.footerBlock2Item4}</h5></a>
        <a href="https://etherscan.io/address/0xada203072fe674c878c18f44cf1a5411921124f9#code" target="blank"><h5 className="items">{strings.footerBlock2Item5}</h5></a>
      </div>
      <div className="child">
        <h4 className="title">{strings.footerBlock3Title}</h4>
        <NavLink className="nav_link" to="/help" ><h5 className="items">{strings.footerBlock3Item1}</h5></NavLink>
        <NavLink className="nav_link" to="/help" ><h5 className="items">{strings.footerBlock3Item2}</h5></NavLink>
      </div>
      <div className="child">
        <h4 className="title">{strings.footerBlock4Title}</h4>
        <a href="https://www.stateofthedapps.com/" target="blank"><h5 className="items">{strings.footerBlock4Item1}</h5></a>
      </div>
    </footer>
  );
};

export default Footer;
