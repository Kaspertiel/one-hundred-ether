import React from 'react';
import Modal from 'react-responsive-modal';
import '../styles/modal.css';
import {Button} from 'react-bootstrap';
import * as strings from '../config/strings';

class InfoModal extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      open: false,
    };
    this.onOpenModal = this.onOpenModal.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
  }

  render() {
    const open = this.state.open;
    return (
      <div className="inner">
        <Button className="btn-info" onClick={this.onOpenModal}><i className="fas fa-info"></i></Button>
        <Modal open={open} onClose={this.onCloseModal} little>
          <h2 className="modalH2">{strings.infoModalTitle}</h2>
          <h5>{strings.infoModalExp1}</h5>
          <h5>{strings.infoModalExp2}</h5>
          <h5>{strings.infoModalExp3}</h5>
          <h5>{strings.infoModalExp4}</h5>
          <h5>{strings.infoModalExp5}{this.props.soldBlocks}</h5>
        </Modal>
      </div>
    );
  }

  onOpenModal() {
    this.setState({ open: true });
  }

  onCloseModal() {
    this.setState({ open: false });
  }
}

export default InfoModal;
