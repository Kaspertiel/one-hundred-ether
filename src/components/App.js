import React from 'react';
import { Switch, NavLink, Route } from 'react-router-dom';
import HomePage from './containers/HomePage';
import AboutPage from './containers/AboutPage';
import HelpPage from './containers/HelpPage';
import MarketPage from './containers/MarketPage';
import Footer from './Footer';
import '../styles/app.css';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/jelly.css';
import * as strings from '../config/strings';

const ScrollToTop = () => {
  window.scrollTo(0, 0);
  return null;
};

class App extends React.Component {

  render() {
    return (
      <div className="App site">
        <div className="site-content">
          <div className="nav">
            <NavLink className="nav_link" exact to="/" >{strings.navLink1}</NavLink>
            {' | '}
            <NavLink className="nav_link" to="/market" >{strings.navLink2}</NavLink>
            {' | '}
            <NavLink className="nav_link" to="/about" >{strings.navLink3}</NavLink>
            {' | '}
            <NavLink className="nav_link" to="/help" >{strings.navLink4}</NavLink>
          </div>
          <Route component={ScrollToTop} />
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/market" component={MarketPage} />
            <Route path="/about" component={AboutPage} />
            <Route path="/help" component={HelpPage} />
          </Switch>
          <Alert stack={{limit: 3}} />
        </div>
        <Footer/>
      </div>
    );
  }
}

export default App;
