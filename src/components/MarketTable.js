import React from 'react';
import {Button} from 'react-bootstrap';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import * as strings from '../config/strings';

const CaptionElement = () => <h3 style={{
  borderRadius: '0.25em',
  textAlign: 'center',
  color: 'black',
  background: 'white',
  border: '1px solid white',
  padding: '0.5em' }}>{strings.marketTableMainTitle}</h3>;

class MarketTable extends React.Component {

	constructor (props) {
    super(props);
    this.handleBuy = this.handleBuy.bind(this);
    this.rankFormatter = this.rankFormatter.bind(this);
    this.rankFormatterImg = this.rankFormatterImg.bind(this);
  }

	render() {
    const columns = [{
      filter: textFilter(),
      dataField: 'title',
      text: strings.marketTableTitleField
    }, {
      filter: textFilter(),
      dataField: 'xy',
      text: strings.marketTablePosField
    }, {
      filter: textFilter(),
      dataField: 'widthHeight',
      text: strings.marketTableSizeField
    }, {
      filter: textFilter(),
      dataField: 'owner',
      text: strings.marketTableOwnerField
    }, {
      dataField: 'price',
      text: strings.marketTablePriceField
    }, {
      dataField: 'preview',
      text: strings.marketTablePreviewField,
      formatter: this.rankFormatterImg,
    }, {
      dataField: 'actions',
      text: strings.marketTableActionsField,
      formatter: this.rankFormatter,
    }];

    let ads = [];
    this.props.images.map((image, i) => {
      if (image.forSale === true) {
        let ad = {
          idx: image.idx,
          key: i,
          title: image.title,
          ipfsHash: image.ipfsHash,
          xy: strings.marketTableXContainer + image.x + strings.marketTableYContainer + image.y,
          widthHeight: strings.marketTableWidthContainer + image.width + strings.marketTableHeightContainer + image.height,
          owner: image.owner,
          price: this.props.web3.fromWei(image.marketPrice, 'ether') + strings.marketTablePriceContainer,
        }
        ads.push(ad);
      }
      return null;
    })
    return (
      <BootstrapTable
        keyField='key'
        data={ ads }
        columns={ columns }
        noDataIndication={strings.marketTableNoAds}
        caption={<CaptionElement />}
        filter={ filterFactory() }
        pagination={ paginationFactory() }
      />
    );
  }

  rankFormatter(cell, row, rowIndex) {
    return (
      <Button onClick={() => this.handleBuy(row)} bsStyle="success" className={ [cell] }>Buy</Button>
    );
  }

  rankFormatterImg(cell, row, rowIndex) {
    return (
      <img src={"https://gateway.ipfs.io/ipfs/" + row.ipfsHash} role="presentation" width="40px" height="40px" className={ [cell] }></img>
    );
  }

  handleBuy(row){
    for (let i = 0; i < this.props.images.length; i++) {
      if (this.props.images[i].idx === row.idx) {
        let selectedImage = this.props.images[i];
        this.props.marketBuy(selectedImage);
      }
    }
  }
}

export default MarketTable;
