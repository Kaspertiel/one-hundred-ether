import React from 'react';
import '../styles/ad-grid.css';

const GridItem = ({
  selected
}) => {
  let classes = selected ? 'item selected' : 'item';
  classes += ' grid-item';
  return (
    <div className={classes}></div>
  );
};

export default GridItem;
