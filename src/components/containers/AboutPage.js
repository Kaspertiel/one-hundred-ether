import React from 'react';
import '../../styles/about-page.css';
import * as strings from '../../config/strings';

const AboutPage = () => {
  return (
    <div className="wrapper">
      <h2> {strings.aboutTitle} </h2>
      <div className="textblock">
        <h4> {strings.underTitle} </h4>
        <p> {strings.exp1} </p>
      </div>

      <div className="textblock">
        <h4> {strings.truffleTitle} </h4>
        <p>{strings.exp2}</p>
        <a href={strings.truffleLink} target="blank"><p>{strings.truffleLinkName}</p></a>
      </div>

      <div className="textblock">
        <h4> {strings.metamaskTitle} </h4>
        <p> {strings.exp3} </p>
        <a href={strings.metaLink} target="blank"><p>{strings.metaLinkName}</p></a>
      </div>

      <div className="textblock">
        <h4> {strings.web3Title} </h4>
        <p>{strings.exp4}</p>
        <a href={strings.web3Link} target="blank"><p>{strings.web3LinkName}</p></a>
      </div>

      <div className="textblock">
        <h4> {strings.ipfsTitle} </h4>
        <p>{strings.exp5}</p>
        <a href={strings.ipfsLink} target="blank"><p>{strings.ipfsLinkName}</p></a>
      </div>
    </div>
  );
};

export default AboutPage;
