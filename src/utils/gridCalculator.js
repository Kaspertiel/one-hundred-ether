const blockPixelSize = 25;

const calculatePos = (index, selectedItems) => {
  let column = selectedItems[index] % 50;
  let row = Math.floor(selectedItems[index] / 50);

  return [column, row];
}

const calculateCoords = (index, selectedItems) => {
  let pos = calculatePos(index, selectedItems);
  let x = pos[0] * blockPixelSize;
  let y = pos[1] * blockPixelSize;
  return [x, y];
}

const calculateSize = (selectedItems) => {
  let startCoords = calculateCoords(0, selectedItems);
  let startX = startCoords[0] - blockPixelSize;
  let startY = startCoords[1] - blockPixelSize;

  let endCoords = calculateCoords(selectedItems.length - 1, selectedItems);
  let endX = endCoords[0];
  let endY = endCoords[1];

  let sizeX = endX - startX;
  let sizeY = endY - startY;

  return [sizeX, sizeY];
}

const calculateBlockPos = (x, y, width, height) => {
  let blocks = [];
  let horizontalBlocks = width / blockPixelSize;
  let verticalBlocks = height / blockPixelSize;
  for(let i = 0; i < horizontalBlocks; i++) {
    for (let a = 0; a < verticalBlocks; a++) {
      let col = (x + (i * blockPixelSize)) / blockPixelSize;
      let row = ((y + (a * blockPixelSize)) / blockPixelSize) * 50;
      let id = col + row;
      blocks.push(id);
    }
  }
  return blocks;
}

export {calculatePos, calculateCoords, calculateSize, calculateBlockPos}
