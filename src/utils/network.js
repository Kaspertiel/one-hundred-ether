const checkNetwork = (network) => {
  var netId = network;
  let networkName;
  let networkLink = null;
  switch (netId) {
    case "1":
      networkName = "Main";
      networkLink = "https://www.ethereum.org/";
      break;
    case "2":
     networkName = "Morden";
     break;
    case "3":
      networkName = "Ropsten";
      networkLink = "http://faucet.ropsten.be:3001/";
      break;
    case "4":
      networkName = "Rinkeby";
      networkLink = "https://www.rinkeby.io/#stats";
      break;
    case "42":
      networkName = "Kovan";
      networkLink = "https://gitter.im/kovan-testnet/faucet";
      break;
    default:
      networkName = "Unknown";
  }
return [networkName, networkLink];
}

export {checkNetwork}
